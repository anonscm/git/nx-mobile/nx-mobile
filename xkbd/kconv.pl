#!/usr/bin/perl

#
# Quick hack to convert old pre 0.5 xkbd config files
# into the newer 'apache' style ones. 
#
# You may have to remove the last empty <row> def 
# for xkbd not to barf .... to fix

use strict;

my $from = shift;
print "<global>\n";
print "</global>\n";
print "<row>\n";

open(IN, "<$from") or die "cant open $from for reading";
while(<IN>)
{
    next if (/^#/ or /^\s*$/);
    if (/^\.$/)
    {
	print "</row>\n";
	print "<row>\n";
	next;
    }
    my @parts = split;
    print "<key>\n";
    print qq|default = "$parts[0]"\n|;
    print qq|default_ks = "$parts[2]"\n|;
    print qq|shift = "$parts[1]"\n|;
    print qq|shift_ks = "$parts[3]"\n|;
    print "</key>\n";
}
print "</row>\n";
close (IN);


