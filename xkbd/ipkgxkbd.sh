#!/bin/bash
VERSION=0.3-1
mkdir -p -m 0755 xkbdbld/usr/bin/
mkdir -p -m 0755 xkbdbld/etc/
mkdir -p -m 0755 xkbdbld/CONTROL/
echo "Package: xkbd
Priority: optional
Version: $VERSION
Architecture: arm
Maintainer: mallum <mallum@handhelds.org> 
Depends: xlibs
Description: xkbd is a small simple xlib based onscreen keyboard
.
" > xkbdbld/CONTROL/control
echo "/etc/xkbd" > xkbdbld/CONTROL/conffiles
cp .keyboard xkbdbld/etc/xkbd
cp xkbd xkbdbld/usr/bin/

mkdir -p xkbdbld/usr/lib/menu/
echo "?package(xkbd):\
needs=\"x11\"\
section=\"Input Methods\"\
title=\"Virtual Keyboard\"\
command=\"/usr/bin/xkbd -o\"" > xkbdbld/usr/lib/menu/xkbd

cat menu_inst.sh > xkbdbld/CONTROL/postinst
chmod +x xkbdbld/CONTROL/postinst
cat menu_inst.sh > xkbdbld/CONTROL/postrm
chmod +x xkbdbld/CONTROL/postrm
cat menu_inst.sh > dumass

ipkg-build xkbdbld ./

### now build the deb

mv  xkbdbld/CONTROL  xkbdbld/DEBIAN
chmod 0755 xkbdbld/DEBIAN
chown root.root xkbdbld/DEBIAN
chown root.root xkbdbld/DEBIAN/*
echo "Package: xkbd
Priority: optional
Version: $VERSION
Architecture: arm
Maintainer: Matthew Allum <mallum@handhelds.org> 
Depends: xlibs
Description: A small simple xlib based onscreen keyboard
" > xkbdbld/DEBIAN/control


dpkg-deb -b xkbdbld "xkbd_${VERSION}_arm.deb"
rm -fr xkbdbld

echo "copying to intimate repos"
ssh mallum@handhelds.org "cd /home/intimate/website/data/debian/dists/unstable/main/binary-arm/x11/ && rm xkbd*"
scp xkbd_${VERSION}_arm.deb mallum@handhelds.org:/home/intimate/website/data/debian/dists/unstable/main/binary-arm/x11/
echo "running mkrelease"
ssh mallum@handhelds.org "cd /home/intimate/website/data/debian/ && ./mkrelease"







