#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
# export DH_VERBOSE=1

# This has to be exported to make some magic below work.
export DH_OPTIONS

# Common path configuration.
confflags += --prefix=/usr --mandir=/usr/share/man

# Common build options.
confflags += --with-mantype=doc
confflags += --with-4in6
confflags += --without-rand-helper

# Options specific to the deb build.
confflags += --with-tcp-wrappers
confflags += --with-ssl-engine

build: build-deb

build-deb: build-deb-stamp
build-deb-stamp:
	dh_testdir
	autoreconf -i -s
	mkdir -p build-deb
	cd build-deb && ../configure $(confflags)

	touch build-deb-stamp

clean:
	dh_testdir
	rm -f build-deb-stamp
	rm -rf build-deb
	$(MAKE) -C contrib clean
	rm -f config.log
	dh_clean

install: DH_OPTIONS=-a
install: build
	dh_testdir
	dh_testroot
	dh_clean -k
	dh_installdirs

	$(MAKE) -C build-deb DESTDIR=`pwd`/debian/openssh-client nxssh
	install -s -m 0755 build-deb/nxssh debian/nxssh/usr/bin


# Build architecture-dependent files here.
binary-arch: binary-openssh-client

binary-openssh-client: build install
	dh_testdir
	dh_testroot
	dh_installdocs
	dh_installchangelogs CHANGELOG
	dh_strip
	dh_compress
	dh_fixperms
	dh_shlibdeps
	dh_gencontrol
	dh_md5sums
	dh_builddeb

binary: binary-arch

.PHONY: build clean binary-indep binary-arch binary install
.PHONY: build-deb build-udeb
.PHONY: binary-openssh-client binary-openssh-server binary-ssh
.PHONY: binary-openssh-client-udeb binary-openssh-server-udeb
